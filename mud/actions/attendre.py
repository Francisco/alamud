from .action import Action1
from mud.events import DeadAction

class WaitAction(Action1):
    EVENT = DeadAction
    ACTION = "wait"