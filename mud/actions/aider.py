from .action import Action2
from mud.events import HelpEvent

class HelpAction(Action2):
    EVENT = HelpEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "help"