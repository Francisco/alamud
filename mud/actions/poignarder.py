from .action import Action3
from mud.events import PoignarderWithEvent



class PoignarderWithAction(Action3):
    EVENT = PoignarderWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "poignard-with"